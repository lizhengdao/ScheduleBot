# ScheduleBot 

## Introduction
🤖 ScheduleBot is a backend for Telegram bots meant to set reminders based on a schedule from a published Google Sheet.

This backend can handle several Telegram bots.\
Each bot can handle several schedules taken from a [published](https://support.google.com/docs/answer/183965) Google Sheet.\
Each bot can have several triggers, [cron expression based](https://crontab.guru/).\
A trigger send a reminder to one chat.

## Configuration file (config.json)
This file must be located in the same directory of the executable.

```bash
├── Bots
|   └── "botname"
│       ├── Token (Your Bot token here)
│       ├── StartMessage (Optional /start message of the bot)
│       ├── Sources
│       │   ├── "sourcename" (Friendly name of the Google Sheet, no spaces)
│       │   │   ├── Id (ID of the Google Sheet. You can find it in the URL. The sheet must be published)
│       │   │   ├── GridNumber (Number of the sheet/tab inside your Google Sheet)
│       │   │   └── DateFormat (Format of the date, the column A. e.g. "dd/MM/yyyy")
│       │   └── ...
│       ├── Triggers
│       │   ├── 0
│       │   │   ├── Cron (Cron expression)
│       │   │   ├── ChatId (The reminder will be sent to this Chat ID)
│       │   │   ├── Command (The command that will be executed)
│       │   │   └── Format (Optional format string to wrap the result. e.g. "Here is your schedule: {0}")
│       │   └── ...
│       ├── AllowedChatIds
│       │   ├── "id": "description" (ID and description of the allowed chats. Only Admin can set the allowed chats and enable the bot to talk within this chat)
│       │   └── ...
│       └── CommandAliases
│           ├── "/alias": "/command" (Alias of a command)
│           └── ...
└── Admins
    ├── "username" (The Telegram username of the admin)
    └── ...
```

See also [config-sample.json](config-sample.json).

## Available commands

### Public commands
* Start: `/start`
* ChatId: `/thischatid`
* BotInfo: `/botinfo`

### Allowed chats only command
* GetScheduleRegex: `^\/([A-Za-z0-9]+):(today|weekstart|monthstart)(([+-])([0-9]+)(d|w|m))?`
    ```
    Capturing groups:
    1. Source name
    2. Reference. Can be one of these: today, weekstart, monthstart
    3. Optional period to add
        1. Sign +/-
        2. Number
        3. What to add (day, week, month)
    ```

* GetSourceRegex: `^\/getsource:([A-Za-z0-9]+)`
    ```
    Capturing group: The source name you want to get the url of
    ```


### Admin only commands
* GetAllowedChats: `/getallowedchats`
* AddAllowedChatIdRegex: `^\/addallowedchatid (-?\d+)`
    ```
    Capturing group: the chat id to add (you can retrieve it with the command /chatid)
    ```
* RemoveAllowedChatIdRegex: `^\/removeallowedchatid (-?\d+)`
    ```
    Capturing group: the chat id to remove
    ```

* GetTriggers: `/gettriggers`
* SetTriggersRegex: `^\/settriggers (.+(?:\n(?!\t).*)*)`
    ```
    Capturing group: a json array of triggers (in the config.json format)
    ```

* GetAliases: `/getaliases`
* SetAliasesRegex: `^\/setaliases (.+(?:\n(?!\t).*)*)`
    ```
    Capturing group: the json array with all the aliases (will replace existing ones)
    ```

## Setup the backend with ScheduleBot

* Create your bot with [BotFather](https://t.me/botfather) and get the _bot token_.
* Clone this project with Visual Studio

### Building for Windows
* Compile the project
* Setup the configuration file
* Launch the bot or create a service with [`sc create`](https://docs.microsoft.com/it-it/windows-server/administration/windows-commands/sc-create)

### Building for [Raspbian](https://www.raspberrypi.org/downloads/raspbian/)

* Compile the project with this command: `dotnet publish -r linux-arm`
* Setup the configuration file
* Launch the bot or create a service with [`systemd`](https://devblogs.microsoft.com/dotnet/net-core-and-systemd/)

## Credits
The bot uses [this .NET Client](https://github.com/TelegramBots/Telegram.Bot) to deal with the [Telegram Bot API](https://core.telegram.org/bots/api).\
It uses [Quartz.NET](https://github.com/quartznet/quartznet) to schedule jobs.
