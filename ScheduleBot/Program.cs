using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ScheduleBot.Helpers;
using System.Collections.Generic;

namespace ScheduleBot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSystemd()
                .ConfigureLogging(logging =>
                {
                    // Settings for file logging
                   logging.AddFile(FileHandling.GetLogFilePath("Log-{Date}.txt"), levelOverrides: new Dictionary<string, LogLevel> {
                        { "Microsoft", LogLevel.Warning },
                        { "System", LogLevel.Warning }
                    });

                    // Settings for console logging
                    logging.AddConsole();
                    logging.AddFilter("Microsoft", LogLevel.Warning);
                    logging.AddFilter("System", LogLevel.Warning);
                })
                .ConfigureServices((hostContext, services) => services.AddHostedService<Worker>());
    }
}
