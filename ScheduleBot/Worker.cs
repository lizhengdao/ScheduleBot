using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;
using ScheduleBot.Models;
using ScheduleBot.Properties;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types.Enums;

namespace ScheduleBot
{
    public class Worker : BackgroundService
    {
        public static Dictionary<string,ITelegramBotClient> botClients;
        private static int ConnectionDelay = 60000;
        public static ILogger<Worker> Logger;
        private static IScheduler sched;
        public static AppConfig Config;
        public static List<JobKey> pendingJobs = new List<JobKey>();

        public Worker(ILogger<Worker> logger)
        {
            Logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // Begin
            Logger.LogInformation(string.Format(Strings.bot_started, Assembly.GetEntryAssembly().GetName().Version.ToString()));

            try
            {
                // Load the configuration
                Config = AppConfig.Load();

                // Init Telegram Bot Clients
                botClients = new Dictionary<string,ITelegramBotClient>();
                foreach(KeyValuePair<string, Bot> bot in Config.Bots)
                {
                    // Instantiate bot
                    TelegramBotClient botClient = new TelegramBotClient(bot.Value.Token);

                    // Listen for messages
                    botClient.OnMessage += Bot_OnMessage;
                    botClient.OnReceiveGeneralError += BotClient_OnReceiveGeneralError;
                    botClient.StartReceiving();

                    // Add to the list
                    botClients.Add(bot.Key, botClient);
                }

                // Setup the scheduled jobs

                // Construct a scheduler factory
                NameValueCollection props = new NameValueCollection { { "quartz.serializer.type", "binary" } };
                StdSchedulerFactory factory = new StdSchedulerFactory(props);

                // Gt a scheduler
                sched = await factory.GetScheduler();
                await sched.Start();

                // Cycle through all bots and setup the scheduled jobs
                foreach(KeyValuePair<string, Bot> bot in Config.Bots)
                    ScheduleJobs(bot.Key, bot.Value.Triggers);

            }
            catch (FileNotFoundException fnfe)
            {
                Logger.LogError(message: Strings.config_not_found, exception: fnfe);
            }
            catch (ArgumentException aex)
            {
                Logger.LogError(message: Strings.invalid_token, exception: aex);
            }

            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(ConnectionDelay, stoppingToken);
            }

            // End gracefully
            foreach(TelegramBotClient botClient in botClients.Values)
                botClient.StopReceiving();
        }

        /// <summary>
        /// Schedule the triggers for a bot
        /// </summary>
        /// <param name="botName">The bot name</param>
        /// <param name="triggers">The triggers</param>
        public static async void ScheduleJobs(string botName, List<Trigger> triggers)
        {
            // Remove all existing jobs for this bot
            var jobs = sched.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(botName)).Result;
            await sched.DeleteJobs(jobs);

            // Setup triggers
            foreach (Trigger trigger in triggers)
            {
                // Setup job data
                IDictionary<string, object> jobData = new Dictionary<string, object>();
                jobData.Add(ScheduledJob.BotName, botName);
                jobData.Add(ScheduledJob.ChatId, trigger.ChatId);
                jobData.Add(ScheduledJob.Command, trigger.Command);
                jobData.Add(ScheduledJob.Format, trigger.Format);

                // Create the job
                IJobDetail job = JobBuilder.Create<ScheduledJob>()
                            .WithIdentity(getJobKey(botName, triggers.IndexOf(trigger)))
                            .SetJobData(new JobDataMap(jobData))
                            .Build();

                // Create the trigger
                ITrigger tr = TriggerBuilder.Create()
                    .WithIdentity("trigger." + triggers.IndexOf(trigger), botName)
                    .StartNow()
                    .WithCronSchedule(trigger.Cron)
                    .Build();

                // Log
                Logger.LogInformation(string.Format(Strings.scheduled_job,botName,trigger.Cron));

                // Schedule them
                await sched.ScheduleJob(job, tr);
            }
        }

        private static JobKey getJobKey(string botName, int triggerIndex)
        {
            return new JobKey("job." + triggerIndex, botName);
        }

        public static void BotClient_OnReceiveGeneralError(object sender, ReceiveGeneralErrorEventArgs e)
        {
            TelegramBotClient botClient = (TelegramBotClient)sender;
            // If an error occours, stop listening for a while...
            botClient.StopReceiving();
            // ...wait...
            Thread.Sleep(ConnectionDelay);
            // ...and then try to, restart
            botClient.StartReceiving();

            if(sched != null && pendingJobs.Count > 0)
            {
                // Check if you are online again
                try
                {
                    Thread.Sleep(5000);
                    var me = botClient.GetMeAsync().Result;

                    // If no exceptions, resume pending scheduled jobs
                    Logger.LogInformation(Strings.got_back_online);

                    while (pendingJobs.Count > 0)
                    {
                        sched.TriggerJob(pendingJobs[0]);
                        pendingJobs.RemoveAt(0);
                    }
                }
                catch(Exception ex)
                {
                    // not online yet
                    Logger.LogWarning(Strings.not_online_yet);
                }
            }
        }

        public static void Bot_OnMessage(object sender, MessageEventArgs e)
        {
            TelegramBotClient botClient = (TelegramBotClient)sender;
            string botName = botClients.FirstOrDefault(x => x.Value == botClient).Key;
            if (e.Message.Text != null)
            {
                // Process command
                Logic.OnCommand(
                    botName: botName, 
                    chatId: e.Message.Chat.Id,
                    message: e.Message, 
                    command: e.Message.Text);
            }
            else if (e.Message.Document != null)
            {
                // Process file
            }
            else if (e.Message.Type != MessageType.Sticker)
            {
                // If another unhandled type of content
            }
        }
    }
}
