﻿namespace ScheduleBot
{
    class Command
    {
        #region Public commands
        /// <summary>
        /// Get schedule with this command:
        /// Capturing groups:
        ///  1. Source name
        ///  2. Reference. Can be one of these: today, weekstart, monthstart
        ///  3. Optional period to add
        ///     1. Sign +/-
        ///     2. Number
        ///     3. What to add (day, week, month)
        ///</summary>
        public const string GetScheduleRegex = "^\\/([A-Za-z0-9]+):(today|weekstart|monthstart)(([+-])([0-9]+)(d|w|m))?";
        
        /// <summary>
        /// Get the link of a specific source with this command
        /// Capturing group: The source name you want to get the url of
        /// </summary>
        public const string GetSourceRegex = "^\\/getsource:([A-Za-z0-9]+)";

        /// <summary>
        /// Get this chat id
        /// </summary>
        public const string ChatId = "/thischatid";

        /// <summary>
        /// Get infos about ScheduleBot
        /// </summary>
        public const string BotInfo = "/botinfo";

        /// <summary>
        /// Start message
        /// </summary>
        public const string Start = "/start";
        #endregion

        #region Members handling (admin only)
        /// <summary>
        /// List all allowed chats
        /// </summary>
        public const string GetAllowedChats = "/getallowedchats";
        /// <summary>
        /// Add a chat to the allowed ones
        /// Capturing group: the chat id to add (you can retrieve it with the command /chatid)
        /// </summary>
        public const string AddAllowedChatIdRegex = "^\\/addallowedchatid (-?\\d+)";
        /// <summary>
        /// Remove a specific chat id from the allowed ones
        /// Capturing group: the chat id to remove
        /// </summary>
        public const string RemoveAllowedChatIdRegex = "^\\/removeallowedchatid (-?\\d+)";
        #endregion

        #region Triggers handling (admin only)
        /// <summary>
        /// Get active triggeres. Will return the json array with the triggers
        /// </summary>
        public const string GetTriggers = "/gettriggers";
        /// <summary>
        /// Set triggers (json format)
        /// Capturing group: the json array with all the triggers (will replace existing ones)
        /// </summary>
        public const string SetTriggersRegex = "^\\/settriggers (.+(?:\\n(?!\\t).*)*)";
        #endregion

        #region Alias handling (admin only)
        /// <summary>
        /// Get command aliases. Will return the json array with the aliases
        /// </summary>
        public const string GetAliases = "/getaliases";
        /// <summary>
        /// Set aliases (json format)
        /// Capturing group: the json array with all the aliases (will replace existing ones)
        /// </summary>
        public const string SetAliasesRegex = "^\\/setaliases (.+(?:\\n(?!\\t).*)*)";
        #endregion
    }
}
