﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScheduleBot.Helpers
{
    static class DateUtil
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }

        public static DateTime StartOfMonth(this DateTime dt)
        {
            return dt.AddDays(1 - dt.Day).Date;
        }
    }
}
