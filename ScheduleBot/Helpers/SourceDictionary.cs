﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace ScheduleBot.Models
{
    class SourceDictionary
    {
        /// <summary>
        /// This function extrscts in a dictionary the columns A and B of the Google Sheet described in the source
        /// It creates and entry only if both columns A and B are filled in
        /// </summary>
        /// <param name="source">The source of the data</param>
        /// <param name="skipLeadingRows">The number of the leading rows to skil (default=1 for the header)</param>
        /// <returns></returns>
        public static Dictionary<string,string> GetFromSource(Source source, int skipLeadingRows = 1)
        {
            GoogleSheet googleSheet;
            Dictionary<string, string> sourceDictionary = new Dictionary<string, string>();

            try
            {
                // Download and parse Google Sheet from json format
                using (WebClient client = new WebClient())
                {
                    string json = client.DownloadString(source.GetJsonUrl());
                    googleSheet = GoogleSheet.Parse(json);
                }

                // Extract all values and create a base dictionary
                Dictionary<string, string> coordinatesDictionary = new Dictionary<string, string>();
                foreach (Entry entry in googleSheet.feed.entry)
                    coordinatesDictionary.Add(entry.title.t, entry.content.t);

                // Compose a dictionary from column A and B
                foreach (String akey in coordinatesDictionary.Keys)
                {
                    // Check if found an A column
                    Match match = Regex.Match(akey, "^A([0-9]+)");
                    if (match.Success && Int32.Parse(match.Groups[1].Value) > skipLeadingRows)
                    {
                        // Search for the corresponding B value
                        string bvalue = coordinatesDictionary["B" + match.Groups[1].Value];
                        if (bvalue != null)
                            sourceDictionary.Add(coordinatesDictionary[akey], bvalue);
                    }
                }
            }
            catch(Exception e)
            {
                Worker.Logger.Log(LogLevel.Error, e.Message);
            }
            return sourceDictionary;
        }
    }
}
