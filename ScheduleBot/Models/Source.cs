﻿namespace ScheduleBot.Models
{
    /// <summary>
    /// This is a source of information
    /// It's get from a published Google Sheet
    /// </summary>
    public class Source
    {
        /// <summary>
        /// The Google Sheet Id. You can find it in the Google Sheet URL
        /// </summary>
        public string Id { get; set; }
        public int GridNumber { get; set; }
        public string DateFormat { get; set; }

        public string GetJsonUrl()
        {
            return string.Format("https://spreadsheets.google.com/feeds/cells/{0}/{1}/public/basic?alt=json",Id, GridNumber);
        }

        public string GetUrl()
        {
            return string.Format("https://docs.google.com/spreadsheets/d/{0}",Id);
        }
    }
}
