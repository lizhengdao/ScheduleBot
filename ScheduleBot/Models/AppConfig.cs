﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace ScheduleBot.Models
{
    public class AppConfig
    {
		public static string path = Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),"config.json");

		public Dictionary<String, Bot> Bots { get; set; }
        public List<String> Admins { get; set; }

		public void Save()
		{
			var json = JsonConvert.SerializeObject(this, Formatting.Indented);
			File.WriteAllText(path, json);
		}
		public static AppConfig Load()
		{
			var json = File.ReadAllText(path);
			return JsonConvert.DeserializeObject<AppConfig>(json);
		}

		public bool IsAdmin(string adminUsername)
		{
			return Admins != null && Admins.Contains(adminUsername);
		}
	}
}
