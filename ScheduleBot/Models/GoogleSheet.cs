﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ScheduleBot.Models
{
    public class GoogleSheet
    {
        public Feed feed { get; set; }
        public static GoogleSheet Parse(String json)
        {
            return JsonConvert.DeserializeObject<GoogleSheet>(json);
        }
    }

    public class Feed
    {
        public List<Entry> entry { get; set; }
    }

    public class Entry
    {
        public Title title { get; set; }
        public Content content { get; set; }
    }

    public class Title
    {
        [JsonProperty("$t")]
        public String t { get; set; }
    }
    public class Content
    {
        [JsonProperty("$t")]
        public String t { get; set; }
    }

    
}
